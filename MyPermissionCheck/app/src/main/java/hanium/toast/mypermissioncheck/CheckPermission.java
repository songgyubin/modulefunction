package hanium.toast.mypermissioncheck;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public class CheckPermission extends AppCompatActivity {
    private static final String TAG = "CheckPermission";

    /**
     * 체크할 퍼미션 목록
     * manifests.xml과 싱크 맞춤 필요
     */
    public static String[] mpermissions = {
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.CALL_PHONE, // 전화걸기 및 관리
            Manifest.permission.WRITE_CONTACTS, // 주소록 액세스 권한
            Manifest.permission.WRITE_EXTERNAL_STORAGE, // 기기, 사진, 미디어, 파일 엑세스 권한
            Manifest.permission.RECEIVE_SMS, // 문자 수신
            Manifest.permission.CAMERA
    };
    /**
     * 원하는 액티비티에서 import해서 사용
     */
    public static Context context;
    public static Activity activity;

    // 리퀘스트 코드
    public static final int MULTIPLE_PERMISSIONS = 101;

    /**
     * 퍼미션 목록대로 체크
     * @param activity
     * @param context
     * @return
     */
    public static boolean checkPermissions(Activity activity, Context context) {
        int result;
        List<String> permissionList = new ArrayList<>();
        for (String pm : mpermissions) {
            result = ContextCompat.checkSelfPermission(context, pm);
            if (result != PackageManager.PERMISSION_GRANTED) {
                permissionList.add(pm);
            }
        }
        if (!permissionList.isEmpty()) {
            ActivityCompat.requestPermissions(activity, permissionList.toArray(new String[permissionList.size()]), MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


}
